import WebSocket from 'ws';
import jwt from 'jsonwebtoken';
import http from 'http';
import EventEmitter from 'events';

import { strict as assert } from 'assert';

const authenticate = (token, secret, options) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secret, options, (err, decoded) => {
            if (err) {
                return reject(err);
            }
            return resolve(decoded);
        });
    });
};

class Notifications extends EventEmitter {
    constructor(app, server, options) {
        super();
        assert(
            app,
            'express-notifications: must be initialized with a valid express app',
        );

        if (server && !(server instanceof http.Server)) {
            options = server;
            server = null;
        }

        if (!server) {
            server = http.createServer(app);
            app.listen = (...args) => {
                server.listen(...args);
            };
        }

        this.server = server;

        const defaults = {
            path: '/notifications',
            pingInterval: 30,
        };

        this.options = { ...defaults, ...options };

        this._init();
    }

    _handleMessage(ws, data) {
        try {
            const msg = JSON.parse(data);
            if (msg.type === 'auth') {
                if (!msg.data || !msg.data.token) {
                    ws.send(
                        JSON.stringify({
                            type: 'auth',
                            data: {
                                success: false,
                                message: 'Missing parameters',
                            },
                        }),
                    );
                    ws.terminate();
                }

                this.options.jwtSecret &&
                    authenticate(
                        msg.data.token,
                        this.options.jwtSecret,
                        this.options.jwtVerificationOptions,
                    )
                        .then(token => {
                            ws.send(
                                JSON.stringify({
                                    type: 'auth',
                                    data: {
                                        success: true,
                                    },
                                }),
                            );

                            ws.xdata.authenticated = true;
                            ws.user = token;
                            ws.xdata.raw_token = msg.data.token;

                            if (!ws.xdata.authenticated_emitted) {
                                ws.xdata.authenticated_emitted = true;
                                this.emit('client_authenticated', ws);
                            }
                        })
                        .catch(err => {
                            ws.send(
                                JSON.stringify({
                                    type: 'auth',
                                    data: {
                                        success: false,
                                        message: err.message,
                                    },
                                }),
                            );
                            ws.terminate();
                        });
            } else if (msg.type === 'pong') {
                ws.xdata.last_pong = Date.now();
            } else {
                ws.terminate();
            }
        } catch (err) {
            console.error(err);
            ws.terminate();
        }
    }

    _handleError(ws, err) {
        ws.terminate();
    }

    _init() {
        this.wss = new WebSocket.Server({
            server: this.server,
            path: this.options.path,
            clientTracking: true,
        });

        this.wss.on('connection', ws => {
            ws.xdata = {};
            ws.xdata.last_pong = Date.now();
            ws.xdata.authenticated = !this.options.jwtSecret;

            if (ws.xdata.authenticated && !ws.xdata.authenticated_emitted) {
                ws.xdata.authenticated_emitted = true;
                this.emit('client_authenticated', ws);
            }

            ws.on('message', data => this._handleMessage(ws, data));
            ws.on('error', err => this._handleError(ws, err));
        });

        /* periodically check for and remove timeouted or JWT expired clients */
        const dropBadClients = () => {
            if (this.options.pingInterval) {
                const timeout = (this.options.pingInterval + 15) * 1000;
                Array.from(this.wss.clients)
                    .filter(c => c.xdata.last_pong < Date.now() - timeout)
                    .map(c => c.terminate());
            }

            if (this.options.jwtSecret) {
                Array.from(this.wss.clients)
                    .filter(
                        c =>
                            c.user &&
                            c.user.exp &&
                            c.user.exp * 1000 < Date.now(),
                    )
                    .map(c => c.terminate());
            }

            setTimeout(dropBadClients, 10 * 1000);
        };
        dropBadClients();

        /* periodically ping all clients */
        if (this.options.pingInterval) {
            const pingAllClients = () => {
                this.wss.clients.forEach(c =>
                    c.send(JSON.stringify({ type: 'ping' })),
                );

                setTimeout(pingAllClients, this.options.pingInterval * 1000);
            };
            pingAllClients();
        }
    }

    push(type, data, targeted_claims) {
        const msg = JSON.stringify({
            type,
            data,
        });

        Array.from(this.wss.clients)
            .filter(c => c.xdata.authenticated)
            .filter(
                c =>
                    !targeted_claims ||
                    (typeof targeted_claims === 'object' &&
                        Object.keys(targeted_claims).every(
                            claim =>
                                c.user &&
                                c.user[claim] === targeted_claims[claim],
                        )),
            )
            .forEach(c => c.send(msg));
    }
}

export default Notifications;
