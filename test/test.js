import express from 'express';
import WebSocket from 'ws';
import Notifications from '../lib/index';

const app = express();

const notifications = new Notifications(app, {
    jwtSecret: process.env.JWT_SECRET,
});

notifications.on('client_authenticated', ws => {
    ws.send(JSON.stringify({ type: 'welcome', data: 'hello' }));
});

app.listen(8080, () => {
    console.log('Up and running on port 8080...');

    const ws = new WebSocket('ws://localhost:8080/notifications');

    ws.on('open', () => {
        console.log('Connected to server');

        ws.send(
            JSON.stringify({ type: 'auth', data: { token: process.env.JWT } }),
        );
    });

    ws.on('message', msg => {
        console.log(msg);

        const event = JSON.parse(msg);
        if (event.type === 'ping') {
            ws.send(JSON.stringify({ type: 'pong' }));
        }
    });

    ws.on('close', () => {
        console.log('Disconnected from server');
    });
});
