# @allbin/express-notifications

```js
import Notifications from '@allbin/express-notifications';
import express from 'express';

const app = express();
const notifications = new Notifications(app, {
    path: '/message-updates',
    jwtSecret: 'shhhh',
    jwtVerificationOptions: {
        audience: 'https://example.com'
    }
});

app.use(...);

let messages = [];

notifications.on('client_authenticated', (ws) => {
    ws.send({type: "messages:existing", data: messages});
});

app.get('/messages', (req, res, next) => {
    res.status(200).json(messages));
});


app.post('/messages', (req, res, next) => {
    const msg = JSON.parse(req.body);
    messages.push(msg);
    notifications.push('messages:created', [msg]);
    res.status(201);
});

app.listen(process.env.PORT);
```

## Authentication

If `options.jwtSecret` was set during initialization, clients are expected to present credentials
before receiving any notifications. This is done by sending an auth message containing a JWT.

```js
{
    "type": "auth",
    "data": {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
    }
}
```

If the token is accepted by the server, the client will start receiving notification pushes. Note that
the client connection will be terminated if its associated token expires. To avoid service interruption,
the client is therefore expected to periodically acquire new tokens and issue additional "auth" messages
as needed.

## Heartbeats

Connected clients are expected to respond to periodical pings from the server. These will arrive in the following form:

```js
{
    "type": "ping"
}
```

And should simply be responded to with:

```js
{
    "type": "pong"
}
```

## Functions

### Notifications(app, [http_server], [options])

Creates a websocket server with client connection management.

Returns a Notifications object.

Arguments:
`app` - An express app  
`http_server` - An optional instance of a node http.Server. if not supplied, one will be created for you.  
`options` - See below.

Accepted options:
`path` - Route path for the WebSocket server to use. default: `/notifications`  
`pingInterval` - Seconds between WebSocket pings sent to clients, default: `30`  
`jwtSecret` - Passed unchanged to `jsonwebtoken.verify`. default: `undefined`  
`jwtVerificationOptions` - Passed unchanged to jsonwebtoken.verify. default: `undefined`

### notificationInstance.push(type, data, [targeted_claims])

Pushes a notification message to all registered clients in the form:

```js
{
    "type": type,
    "data": data
}
```

Available options:
`type` - a string meant to communicate to clients what type of notification this is.  
`data` - type-dependent data.  
`targeted_claims` - notification will only be sent to authenticated users who possess the specified claims.

## Events

### client_authenticated(ws)

Emitted when a client has successfully authenticated for the first time. Subsequent authentication messages
will not trigger a new emission of this event. It is emitted exactly once per client connection.

If `options.jwtSecret` was not set, this event is emitted immediately upon connection.

If `options.jwtSecret` was set, `ws.user` contains the decoded JWT payload.
